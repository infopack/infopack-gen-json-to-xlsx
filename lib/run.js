var Promise = require('bluebird');
var XLSX = require('xlsx');
var _ = require('lodash');

/**
 * This is the function which effectivly performs the action.
 * @param {*} pipeline Pipeline instance
 * @param {*} settings
 * @returns Promise
 */
function run(pipeline, settings) {
	settings = settings || {};
	settings.fileName = settings.fileName || 'collection';
	settings.storageKey = settings.storageKey || 'collection';

	/**
	 * Optional function which is passed to map
	 */
	if(typeof settings.mapper != 'function') {
		// just send it through
		settings.mapper = (value) => value;
	} 
			
	return Promise
		.resolve()
		.then(() => {
			// needs to be fixed user should be able to control where to read data
			var data = pipeline.getKey(settings.storageKey);
			return _.map(data, settings.mapper);
		})
		.then((tabularData) => {
			var wb = XLSX.utils.book_new();
			var ws = XLSX.utils.json_to_sheet(tabularData);
			XLSX.utils.book_append_sheet(wb,ws,'Data');
			var buff = XLSX.write(wb, { type: 'buffer', bookType: 'xlsx' });
			pipeline.writeFile({
				title: 'Excelfil',
				path: `${pipeline.getOutputDirPath()}/${settings.fileName}.xlsx`,
				data: buff
			});
			// your code here
			return;
		});
}

module.exports = run;
