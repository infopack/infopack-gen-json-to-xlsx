var run = require('./run');
/**
 * This is a convenience method which returns a pipeline step
 * @param {Object} settings Settings object specific to each generator
 * @return Step
 */
function step(settings) {
	return {
		settings: settings || {},
		run: run
	};
};

module.exports = step;
